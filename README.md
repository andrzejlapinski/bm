# Instrukcja
### Backend
Z użyciem virtualenv (lub globalnie)
```sh
$ cd bm
$ pip install -r requirements.txt
```
Edycja pliku bm/backend/settings.py odpowiednimi danymi:
```sh
...
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'myproject',
        'USER': 'myprojectuser',
        'PASSWORD': 'password',
        'HOST': 'localhost',
        'PORT': '',
    }
}
...
```

Migracja baz danych i utworzenie użytkownika bazy danych:
```sh
$ cd backend
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py createsuperuser
```
Uruchomienie backendu:
```sh
python manage.py runserver
```
### Frontend
Wymaga instalacji node + npm
```sh
$ cd ../frontend
$ npm install
$ npm start
```