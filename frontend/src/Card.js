import React, {Component} from "react";
//import { Table, TBody, Td, Th, THead, Tr } from "react-simple-sort-table/src/ReactSimpleSortTable";

class Card extends Component {
    state = {
        menu_card: []
    };

    async componentDidMount() {
        try {
            console.log('Did mount');
            var url = 'http://127.0.0.1:8000/api/' + this.props.match.params.id
            // console.log(url);
            const res = await fetch(url);
            const menu_card = await res.json();
            this.setState({
                menu_card,
            });
        } catch (e) {
            console.log(e);
        }
    }


    render() {
        var menu_card_data = this.state.menu_card;
        return (
            <table>
                <thead>
                <tr>
                    <th>id</th>
                    <th>Nazwa</th>
                    <th>Opis</th>
                    <th>Data dodania</th>
                    <th>Data modyfikacji</th>
                    <th>Potrawa wegetariańska</th>
                    <th>Czas przygotowania</th>
                </tr>
                </thead>
                <tbody>
                {menu_card_data.map(item => (
                        <tr key={item.id}>
                            <td>{item.id}</td>
                            <td>{item.item_name}</td>
                            <td>{item.descr}</td>
                            <td>{item.added_date}</td>
                            <td>{item.modded_date}</td>
                            <td>{item.is_vege}</td>
                            <td>{item.prep_time}</td>
                        </tr>
                    )
                )}
                </tbody>
            </table>
        );
    }
}

export default Card;