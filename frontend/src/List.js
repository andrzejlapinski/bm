import React, {Component} from "react";
//import { Table, TBody, Td, Th, THead, Tr } from "react-simple-sort-table/src/ReactSimpleSortTable";

import {
    NavLink
} from "react-router-dom";

class List extends Component {
    state = {
        menu_list: []
    };

    async componentDidMount() {
        try {
            const res = await fetch('http://127.0.0.1:8000/api/');
            const menu_list = await res.json();
            this.setState({
                menu_list,
            });
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        var menu_list_data = this.state.menu_list;
        return (

            <div>
                <table>
                    <thead>
                    <tr>
                        <th>id</th>
                        <th>Nazwa</th>
                        <th>Opis</th>
                        <th>Data dodania</th>
                        <th>Data modyfikacji</th>
                        <th>Liczba dań</th>
                    </tr>
                    </thead>
                    <tbody>
                    {menu_list_data.map(item => (
                            <tr key={item.id}>
                                <td>{item.id}</td>
                                <td><NavLink to={"/" + item.id}>{item.menu_name}</NavLink></td>
                                <td>{item.descr}</td>
                                <td>{item.added_date}</td>
                                <td>{item.modded_date}</td>
                                <td>{item.count_dish}</td>
                            </tr>
                        )
                    )}
                    </tbody>
                </table>
            </div>

        );
    }

}

export default List;