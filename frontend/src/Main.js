import React, {Component} from "react";
import {
    Route,
    Switch,
    BrowserRouter as Router,
} from "react-router-dom";
import Card from "./Card";
import List from "./List";

class Main extends Component {
    render() {
        return (
            <Router>
                <div>
                    <Switch>
                        <Route exact path="/" component={List}/>
                        <Route path="/:id" component={Card}/>
                    </Switch>
                </div>
            </Router>
        );
    }
}

export default Main;