from django.contrib import admin
from .models import MenuItem, MenuCard



admin.site.register(MenuCard)
admin.site.register(MenuItem)