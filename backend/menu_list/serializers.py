from rest_framework import serializers
from . import models



class MenuItemSerializer(serializers.ModelSerializer):
    menucard = models.MenuCard
    class Meta:
        fields = (
            'id',
            'menucard',
            'item_name',
            'descr',
            'added_date',
            'modded_date',
            'is_vege',
            'prep_time',
            'picture',
        )
        model = models.MenuItem

class MenuCardSerializer(serializers.ModelSerializer):
    count_dish = serializers.SerializerMethodField()
    class Meta:
        model = models.MenuCard
        fields = (
            'id',
            'menu_name',
            'descr',
            'added_date',
            'modded_date',
            'count_dish',
            # 'sub',
        )


    def get_count_dish(self, obj):
        return models.MenuItem.objects.filter(menucard=obj).count()