from django.apps import AppConfig


class MenuListConfig(AppConfig):
    name = 'menu_list'
