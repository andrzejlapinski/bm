# Generated by Django 2.0.5 on 2018-05-02 14:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('menu_list', '0002_auto_20180502_1316'),
    ]

    operations = [
        migrations.AlterField(
            model_name='menuitem',
            name='picture',
            field=models.ImageField(blank=True, upload_to='pictures/'),
        ),
    ]
