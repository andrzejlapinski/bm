from rest_framework import generics, status, viewsets

from . import models
from . import serializers

class MenuCardView(generics.ListCreateAPIView):
    queryset = models.MenuCard.objects.all()
    serializer_class = serializers.MenuCardSerializer

class MenuItemView(generics.ListCreateAPIView):
    # queryset = models.MenuItem.objects.all()
    serializer_class = serializers.MenuItemSerializer
    def get_queryset(self, *args, **kwargs):
        menucard = self.kwargs['pk']
        return models.MenuItem.objects.filter(menucard=menucard)