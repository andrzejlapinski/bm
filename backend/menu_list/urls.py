from django.urls import path
from . import views

urlpatterns = [
    path('', views.MenuCardView.as_view()),
    path('<int:pk>/', views.MenuItemView.as_view())
]