# -*- coding: utf-8 -*-

from django.db import models
from django.utils import timezone
from django.core.validators import MinValueValidator, MaxValueValidator

class MenuCard(models.Model):
    menu_name = models.CharField(unique=True, max_length=50)
    descr = models.CharField(max_length=250)
    added_date = models.DateTimeField('card added date', default=timezone.now, editable=False)
    modded_date = models.DateTimeField('card modded date', editable=False, auto_now=True)

    def __unicode__(self):
        return self.menu_name

    def __str__(self):
        return self.menu_name

class MenuItem(models.Model):
    menucard = models.ForeignKey(MenuCard, on_delete=models.CASCADE)
    item_name = models.CharField(max_length=50)
    descr = models.CharField(max_length=250)
    added_date = models.DateTimeField('card added date', default=timezone.now, editable=False)
    modded_date = models.DateTimeField('card modded date', editable=False, auto_now=True)
    is_vege = models.BooleanField(default=False)
    prep_time = models.IntegerField('dish preparatrion time in minutes', validators=[MinValueValidator(1), MaxValueValidator(60)])
    picture = models.ImageField(upload_to='pictures/', blank=True)

    def __unicode__(self):
        return self.item_name