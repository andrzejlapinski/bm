from django.test import TestCase
from .models import MenuItem, MenuCard
from time import sleep

class MenuCardModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        MenuCard.objects.create(menu_name="Karta 1", descr="Testowy opis 1")

    def test_menu_card_name(self):
        menucard = MenuCard.objects.get(id=1)
        expected_object_name = f'{menucard.menu_name}'
        self.assertEquals(expected_object_name, "Karta 1")

    def test_menu_card_description(self):
        menucard = MenuCard.objects.get(id=1)
        expected_object_name = f'{menucard.descr}'
        self.assertEquals(expected_object_name, "Testowy opis 1")

class MenuItemModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        menucard = MenuCard.objects.create(menu_name="Karta 1", descr="Testowy opis 1")
        MenuItem.objects.create(item_name="Danie 1", menucard=menucard, descr="Testowe danie 1", is_vege=False, prep_time=3)

    def test_menu_item_name(self):
        menuitem = MenuItem.objects.get(id=1)
        expected_object_name = f'{menuitem.item_name}'
        self.assertEquals(expected_object_name, "Danie 1")
